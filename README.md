# Introduction
'Someappxyz' is a template for an Erlang Application which uses some common features in OTP and Yaws.

Built and tested with erlang-OTP-24.0. Earlier versions will not pass Dialyzer checks.

# Overview
The application starts a supervisor which in turn starts a gen_server.

The gen_server creates an ets-table, a dets-table and a mnesia database.

A few light-weight (but OTP-compliant) processes are created which can be used for various tasks.

The application demonstrates a typical usage case where the gen_server is mainly used for initializing and synchronizing read/writes to ets/database tables.

The light-weight processes are used to crunch data and handle concurrent activities.

Read/writes to data in ets/database tables could also be allowed from the light-weight processes provided that a process is only allowed to access its own data entries.

The application can also be started from Yaws which demonstrates a simple REST webserver.

# Usage

## Compile
(cd src;erlc -o ../ebin *.erl)

_Same thing but using the Makefile:_

make

## Run
(cd ebin;erl -run someappxyzMain start_app)

_Same thing but using the Makefile:_

make run_s

_Start the application from within an Erlang shell:_

(cd ebin;erl)

application:start(someappxyz).

_Stop the application:_

application:stop(someappxyz).

# Debugging
## Check the code with dialyzer for type errors
dialyzer --build_plt --apps erts kernel stdlib mnesia eunit xmerl # Only needs to be run once

dialyzer src/*.erl

## Read the logs if Erlang is running as a daemon:
less logs/erlang.log.1

# Application files and directories
## src
Erlang source files (.erl and .hrl).

### src/someappxyz.hrl
Contains record definitions and various macros, for example to enable/disable debug printouts.


### src/someappxyzMain.erl
Entry point to the application.

### src/someappxyzGenserver.erl
Example gen_server for the application

### src/someappxyzProc.erl
Light weight process

### src/someappxyzSupervisor.erl
Top level supervisor for the application

## ebin
Compiled erlang files (.beam) and the application configuration file someappxyz.app.

## app
Application source files which are not compiled but used in other ways.

### app/someappxyz.appSrc
Application configuration file which is copied to ebin/someappxyz.app when the application is built.

### app/overview.edocSrc
Documentation file which is copied to doc/overview.edoc when the documentation is built.

## Makefile
Top level makefile which builds the application. Can also (among other things) generate documentation and run tests.

Makefile targets:

* make / make all - Build everything
* make erlang - Compile all erlang modules and create the .app file from .appSrc
* make prepare_test - Prepare test environment if needed.
* make test - Run tests excluding Yaws code. Ignore errors from dialyzer.
* make test_yaws - Run all tests. Ignore errors from dialyzer.
* make run_s - Run the application in an interactive Erlang shell
* make run_d - Run the application in a detached (from the shell) erlang node
* make run_yaws_s - Run the application with Yaws in an interactive Erlang shell
* make run_yaws_d - Run the application with Yaws in a detached (from the shell) erlang node
* make check_yaws_config - Check the config of a running yaws system with id=myname
* make to_erl - Attach to the erlang shell when the application has been started with 'make run_d'
* make to_yaws - Attach to the erlang shell when the application (and Yaws) has been started with 'make run_yaws_d'
* make priv/serial - Build erlang-serial port
* make kill - Terminate the application
* make doc - Generate documentation
* make clean - Delete all compiled output files
* make clean_doc - Delete all document output files

## priv
Non erlang code and binaries.

## include
Include file(s) exported by the application which can be used by other applications.
