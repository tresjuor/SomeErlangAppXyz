# APP_NAME: Application name
APP_NAME = someappxyz

# LOG_DIR: Directory for the detached shell pipe and to hold the erlang logfiles:
LOG_DIR = ./logs

# Erlang node name and cookie
NODE = xyz@some.org
COOKIE = SomeSecretValue

TEST_NODE = dut@some.org

# Erlang shell pipe
PIPE_ID = 1

# C compiler options
WARNING_OPTIONS =
LANGUAGE_OPTIONS =
COMPILER_OPTIONS = -g

CFLAGS   = $(WARNING_OPTIONS) $(LANGUAGE_OPTIONS) $(COMPILER_OPTIONS)

HEADER_FILES = c_src/serial.h
SOURCE_FILES = c_src/serial.c

OBJECT_FILES = $(SOURCE_FILES:.c=.o)

.PHONY:all erlang prepare_test test run_s run_d to_erl kill doc clean
.PHONY:clean_doc run_yaws_s run_yaws_d check_yaws_config to_yaws test_yaws_rest

# Build everything
all: clean clean_doc erlang doc priv/serial
	@echo --- Target --- $@

# Compile all erlang modules and create the .app file from .appSrc
erlang: clean
	@echo --- Target --- $@
	erlc -W -o ebin/ src/*.erl
	cp app/$(APP_NAME).appSrc ebin/$(APP_NAME).app

# Prepare test environment if needed
prepare_test:
	@echo --- Target --- $@
	dialyzer --build_plt --apps erts kernel stdlib mnesia eunit xmerl

# Run tests excluding Yaws code. Ignore errors from dialyzer.
# Start an erlang node with nodename $(TEST_NODE)
test:
	@echo --- Target --- $@
	-dialyzer src/*.erl
	rm -rf ./test/someappxyz_mnesia_db/dut\@some.org
	rm -rf ./test/someappxyz_dets_tables/dut\@some.org
	erl -pa ./ebin -name $(TEST_NODE) -setcookie $(COOKIE) erl -run $(APP_NAME)Test run_tests

# Run all tests. Ignore errors from dialyzer.
# Start Yaws with nodename $(TEST_NODE)
test_yaws:
	@echo --- Target --- $@
	-dialyzer src/*.erl
	rm -rf ./test/someappxyz_mnesia_db/dut\@some.org
	rm -rf ./test/someappxyz_dets_tables/dut\@some.org
	yaws -i --debug --conf ./yaws/yaws.conf --runmod someappxyzTest --name $(TEST_NODE) --setcookie $(COOKIE) --erlarg "-pa ./ebin +C multi_time_warp +pc unicode"

# Run the application in an interactive Erlang shell
run_s:
	@echo --- Target --- $@
	erl -pa ./ebin -name $(NODE) -setcookie $(COOKIE) -run $(APP_NAME)Main start_app

# Run the application in a detached (from the shell) erlang node
# The erlang logs will be at $(LOG_DIR)/erlang.log.X
run_d:
	@echo --- Target --- $@
	-mkdir -p $(LOG_DIR)
#	RUN_ERL_LOG_MAXSIZE: Optionally set the logfiles maximum size (in bytes) before starting a new log file:
	export RUN_ERL_LOG_MAXSIZE=1000000; run_erl -daemon $(LOG_DIR)/$(PIPE_ID) $(LOG_DIR) "erl -pa ./ebin -name $(NODE) -setcookie $(COOKIE) -run $(APP_NAME)Main start_app"


# Attach to the erlang shell when the application has been started with 'make run_d'
to_erl:
	@echo --- Target --- $@
	to_erl $(LOG_DIR)/$(PIPE_ID)

# Run the application with Yaws in an interactive Erlang shell
run_yaws_s:
	@echo --- Target --- $@
	yaws -i --debug --conf ./yaws/yaws.conf --runmod someappxyzYaws --name $(NODE) --setcookie $(COOKIE) --erlarg "-pa ./ebin +C multi_time_warp +pc unicode"


# Run the application with Yaws in a detached (from the shell) erlang node
# The erlang logfiles will be at /usr/var/log/yaws/erlang-log/$(PIPE_ID)
# Must be run as root unless you hack /usr/bin/yaws and change vardir to something else than "/usr/var".
run_yaws_d:
	@echo --- Target --- $@
	export RUN_ERL_LOG_MAXSIZE=1000000;yaws --daemon --run_erl $(PIPE_ID) --conf ./yaws/yaws.conf --name $(NODE) --setcookie $(COOKIE) --erlarg "-pa ./ebin +C multi_time_warp +pc unicode"

# Check the config of a running yaws system with id=myname
check_yaws_config:
	@echo --- Target --- $@
	yaws --id myname --running-config

# Attach to the erlang shell when the application (and Yaws) has been started with 'make run_yaws_d'
to_yaws:
	@echo --- Target --- $@
	yaws --to_erl $(PIPE_ID)

# Build erlang-serial port
priv/serial: $(OBJECT_FILES)
	@echo --- Target --- $@
	$(CC) -o $@ $(LDFLAGS) $(OBJECT_FILES) $(LDLIBS)

serial.o: serial.c serial.h

# Terminate the application
kill:
	@echo --- Target --- $@
	-pkill -9 beam

# Generate docs
doc:	clean_doc
	@echo --- Target --- $@
	cp app/overview.edocSrc doc/overview.edoc
	erl -eval 'edoc:application($(APP_NAME), ".", []).' -s erlang halt

# Delete all compiled output files
clean:	
	@echo --- Target --- $@
	rm -rf priv/serial c_src/*.o $(OBJECT_FILES) ebin/*.beam ebin/erl_crash.dump ebin/$(APP_NAME).app

# Delete all document output files
clean_doc:
	@echo --- Target --- $@
	rm -rf doc/*
