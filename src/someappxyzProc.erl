%% ====================================================================
%%
%% Description: Lightweight processes
%%
%%
%% License: MIT
%%
%% ====================================================================
% %
% % Copyright (c) 2022 Magnus Uppman <magnus.uppman@tresjuor.se>
% %
% % Permission is hereby granted, free of charge, to any person obtaining a copy
% % of this software and associated documentation files (the "Software"), to deal
% % in the Software without restriction, including without limitation the rights
% % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% % copies of the Software, and to permit persons to whom the Software is
% % furnished to do so, subject to the following conditions:
% %
% % The above copyright notice and this permission notice shall be included in all
% % copies or substantial portions of the Software.
% %
% % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% % SOFTWARE.

%% ====================================================================
%% Module
%% ====================================================================

-module(someappxyzProc).

%% ====================================================================
%% Behaviours
%% ====================================================================

%% ====================================================================
%% Include files
%% ====================================================================

-include("someappxyz.hrl").

-include_lib("eunit/include/eunit.hrl").


%% ====================================================================
%% Internal macros
%% ====================================================================

%% ====================================================================
%% Internal records
%% ====================================================================


%% ====================================================================
%% Function declarations
%% ====================================================================

%% --------------------------------------------------------------------
%% External exported functions
%% --------------------------------------------------------------------

-export([start_new_proc/1, start_new_proc/2]).

% OTP system callbacks
-export([system_continue/3, system_terminate/4]).

% Debug trace formatting
-export([debug/3, debug_str/3]).


%% --------------------------------------------------------------------
%% Internal exported functions
%% --------------------------------------------------------------------

-export([init/3 ]).

%% --------------------------------------------------------------------
%% Imported functions
%% --------------------------------------------------------------------
-import(someappxyzMain, [print_debug/4]).

% Gen_server imports
-import(someappxyzGenserver, [call/3, call/4, cast/3]).

% Imported test functions
-import(someappxyzTest, [write_dets/2, read_dets/1, write_ets/2, delete_dets/1, read_ets/1, delete_ets/1, write_mnesia/2, read_mnesia/1, dirty_read_mnesia/1]).
-import(someappxyzTest, [write_dets_int/2, read_dets_int/1, write_ets_int/2, delete_dets_int/1, read_ets_int/1, delete_ets_int/1,
            write_mnesia_int/2, read_mnesia_int/1, dirty_read_mnesia_int/1]).


%% ====================================================================
%% Function definitions
%% ====================================================================


%% --------------------------------------------------------------------
%% External exported functions
%% --------------------------------------------------------------------

%% --------------------------------------------------------------------
%% @doc
%% Spawns a new lightweight process
%% @end
%% --------------------------------------------------------------------
-spec start_new_proc(Data) -> Result when
    Data    ::  any(),
    Result  ::  {ok, pid()} | {error, Reason :: term()}.
start_new_proc(Data) ->
    start_new_proc(Data, infinity).

%% --------------------------------------------------------------------
%% @doc
%% Spawns a new lightweight process which will timeout at every Timeout ms.
%% @end
%% --------------------------------------------------------------------
-spec start_new_proc(Data, Timeout) -> Result when
    Data    ::  any(),
    Timeout ::  non_neg_integer() | infinity,
    Result  ::  {ok, pid()} | {error, Reason :: term()}.
start_new_proc(Data, Timeout) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("Data: ~p~nTimeout: ~p~n", [Data, Timeout])),
    proc_lib:start_link(?MODULE, init, [Data, self(), Timeout]).

%% --------------------------------------------------------------------
%% @doc
%% Callback function from sys.
%% @end
%% --------------------------------------------------------------------
-spec system_continue(Parent, NewDebugFlags, LoopData) -> no_return() when
    Parent          ::  pid(),
    NewDebugFlags   ::  [any()],
    LoopData        ::  tuple().
system_continue(Parent, NewDebugFlags, {Data, Parent, Timeout, _DebugFlags} = _LoopData) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE),
    NewLoopData = {Data, Parent, Timeout, NewDebugFlags},
    loop(NewLoopData).


%% --------------------------------------------------------------------
%% @doc
%% Callback function from sys.
%% @end
%% --------------------------------------------------------------------
-spec system_terminate(Reason, Parent, DebugFlags, LoopData) -> no_return() when
    Reason          ::  term(),
    Parent          ::  pid(),
    DebugFlags      ::  [any()],
    LoopData        ::  tuple().
system_terminate(Reason, _Parent, _DebugFlags, _LoopData) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE),
    exit(Reason).



%% --------------------------------------------------------------------
%%% Internal exported functions
%% --------------------------------------------------------------------


%% --------------------------------------------------------------------
%% @doc
%% Lightweight process initialization
%% @end
%% --------------------------------------------------------------------
-spec init(Data, Parent, Timeout) -> no_return() when
    Data    ::  any(),
    Parent  ::  pid(),
    Timeout ::  non_neg_integer() | infinity.
init(Data, Parent, Timeout) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("Parent: ~p~nData: ~p~nTimeout: ~p~n", [Parent, Data, Timeout])),
    DebugFlags = sys:debug_options([]), % No debug as default
    proc_lib:init_ack({ok, self()}),
    loop({Data, Parent, Timeout, DebugFlags}).


%% --------------------------------------------------------------------
%%% Internal functions
%% --------------------------------------------------------------------

%% --------------------------------------------------------------------
%% Lightweight process loop
%% --------------------------------------------------------------------
loop({Data, Parent, Timeout, DebugFlags} = LoopData) ->
    receive
        {system, From, Msg} -> % System message
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("From: ~p~nMsg: ~p~nData: ~p~n", [From, Msg, Data])),
            sys:handle_system_msg(Msg, From, Parent, ?MODULE, DebugFlags, LoopData);
        Msg ->
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("Msg: ~p~nData: ~p~n", [Msg, Data])),
            {NewData, NewTimeout} = handle_proc_msg(Msg, Data, Timeout),
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("NewData: ~p~n", [NewData])),

            %% Comment sys:handle_debug if you don't need to trace the process.
            NewDebugFlags = sys:handle_debug(DebugFlags, fun debug/3, {?MODULE, ?FUNCTION_NAME, ?LINE}, {{Msg, Data, Timeout}, {NewData, NewTimeout}}),

            loop({NewData, Parent, NewTimeout, NewDebugFlags})
        after Timeout ->
            %?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("Data: ~p~n", [Data])),
            {NewData, NewTimeout} = handle_proc_timeout(Data, Timeout),
            %?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("NewData: ~p~nNewTimeout: ~p~n", [NewData, NewTimeout])),

            %% Comment sys:handle_debug if you don't need to trace the process.
            NewDebugFlags = sys:handle_debug(DebugFlags, fun debug/3, {?MODULE, ?FUNCTION_NAME, ?LINE}, {{Data, Timeout}, {NewData, NewTimeout}}),

            loop({NewData, Parent, NewTimeout, NewDebugFlags})
	end.



%% --------------------------------------------------------------------
%% @doc
%% Handle incoming messages from lightweight processes.
%%
%% Returns new loop data and new timeout value.
%% @end
%% --------------------------------------------------------------------
-spec handle_proc_msg(Msg, Data, Timeout) -> Result when
    Msg     ::  any(),
    Data    ::  any(),
    Timeout ::  non_neg_integer(),
    Result  ::  {NewData::any(), NewTimeout::non_neg_integer()}.
handle_proc_msg(Msg, Data, Timeout) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, {Msg, Data, Timeout}),
    % Do something

    case Msg of
        q ->
            exit(normal);
%         test_bugg ->
%             1=2;
        {open_serial_port, Speed, Device} ->
            process_flag(trap_exit, true), % We want all exit messages, including "normal".
            SerialPort = serial:start([{speed, Speed},{open, Device}]),
            {SerialPort, 500};
        {send, SerialData} ->
            Data ! {send, SerialData},
            {Data, 500};
        {SerialPort, data, RxData} ->
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("~s~n", [RxData])),
            {SerialPort, 500};
        {'EXIT', Data, _Reason} -> % Serial port has closed
            {port_closed, infinity};
        mnesia ->
            % Test read and write to mnesia with a process switch to the gen_server
            MnesiaKey = 43,
            MnesiaData = #{a_tuple => {q,w,e,r,t,y}, an_int => 123, a_list => [j,k,l]},
            ok = write_mnesia(MnesiaKey, MnesiaData),
            {TimeRead, {ok, [MnesiaRec]}} = timer:tc(fun() -> read_mnesia(43) end),
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("TimeRead: ~p MnesiaRec: ~p~n", [TimeRead, MnesiaRec])),
            {DirtyTimeRead, {ok, [MnesiaRec]}} = timer:tc(fun() -> dirty_read_mnesia(43) end),
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("DirtyTimeRead: ~p MnesiaRec: ~p~n", [DirtyTimeRead, MnesiaRec])),

            % Read from mnesia without involving the gen_server
            {TimeRead_2, {ok, [MnesiaRec]}} = timer:tc(fun() -> read_mnesia_int(43) end),
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("TimeRead_2: ~p MnesiaRec: ~p~n", [TimeRead_2, MnesiaRec])),
            {DirtyTimeRead_2, {ok, [MnesiaRec]}} = timer:tc(fun() -> dirty_read_mnesia_int(43) end),
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("DirtyTimeRead_2: ~p MnesiaRec: ~p~n", [DirtyTimeRead_2, MnesiaRec])),
            {Data, Timeout};
        _ ->
            {Data, Timeout}
    end.





%% --------------------------------------------------------------------
%% @doc
%% Handle timeouts from lightweight processes.
%%
%% Returns new loop data and new timeout value.
%% @end
%% --------------------------------------------------------------------
-spec handle_proc_timeout(Data, Timeout) -> Result when
    Data    ::  any(),
    Timeout ::  non_neg_integer(),
    Result  ::  {NewData::any(), NewTimeout::non_neg_integer()}.
handle_proc_timeout(Data, Timeout) ->
    %?DBG(?MODULE, ?FUNCTION_NAME, {Data, Timeout}),
    % Do something
    {Data, Timeout}.


%% @doc
%% Trace printout and formatting
%% @end
debug(Dev, Info, {Module, Function, Line}) ->
    % io:format("Dev: ~p~n", [Dev]),
    {A,B,C} = erlang:timestamp(),
    io:format(Dev, "~nTRACE: ~p ~p:~p ~p:~p ~p~n~p~n", [self(), calendar:now_to_local_time({A,B,C}), C, Module, Function, Line, Info]).

%% @doc
%% Trace printout and formatting
%% @end
debug_str(Dev, Info, {Module, Function, Line}) ->
    {A,B,C} = erlang:timestamp(),
    io:format(Dev, "~nTRACE: ~p ~p:~p ~p:~p ~p~n~s~n", [self(), calendar:now_to_local_time({A,B,C}), C, Module, Function, Line, Info]).


%% --------------------------------------------------------------------
%%% Test functions
%% --------------------------------------------------------------------

%%% Eunit tests
debug_test_() ->
    [?_test(?assert(handle_proc_msg(any_term, {some_other_term, []}, 1000) =:=  {{some_other_term, []}, 1000})),
    ?_test(?assert(handle_proc_timeout({some_other_term, []}, 2000) =:=  {{some_other_term, []}, 2000}))].

