%% ====================================================================
%%
%% Description: Entry point to the application.
%% A supervisor is started which in turn starts a gen_server.
%%
%% License: MIT
%%
%% ====================================================================
% %
% % Copyright (c) 2022 Magnus Uppman <magnus.uppman@tresjuor.se>
% %
% % Permission is hereby granted, free of charge, to any person obtaining a copy
% % of this software and associated documentation files (the "Software"), to deal
% % in the Software without restriction, including without limitation the rights
% % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% % copies of the Software, and to permit persons to whom the Software is
% % furnished to do so, subject to the following conditions:
% %
% % The above copyright notice and this permission notice shall be included in all
% % copies or substantial portions of the Software.
% %
% % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% % SOFTWARE.

%% ====================================================================
%% Module
%% ====================================================================

-module(someappxyzMain).

%% ====================================================================
%% Behaviours
%% ====================================================================

-behaviour(application).

%% ====================================================================
%% Include files
%% ====================================================================
-include("someappxyz.hrl").

-include_lib("eunit/include/eunit.hrl").


%% ====================================================================
%% Internal macros
%% ====================================================================

%% ====================================================================
%% Internal records
%% ====================================================================


%% ====================================================================
%% Function declarations
%% ====================================================================

%% --------------------------------------------------------------------
%% External exported functions
%% --------------------------------------------------------------------

% Behavioural exports
-export([start/2, stop/1]).

% Application specific exports
-export([start_app/0, stop_app/0, app_init/1, app_terminate/0,
          print_debug/4]).


%% --------------------------------------------------------------------
%% Internal exported functions
%% --------------------------------------------------------------------


%% --------------------------------------------------------------------
%% Imported functions
%% --------------------------------------------------------------------

% Supervisor imports
-import(someappxyzSupervisor, [start_supervisor_link/1]).


% Gen_server imports
-import(someappxyzGenserver, [call/3, call/4, cast/3]).


% Ligtweight proc imports
-import(someappxyzProc, [start_new_proc/1, start_new_proc/2]).


% Test functions
-import(someappxyzTest, [happy_testing/0]).

%% ====================================================================
%% Function definitions
%% ====================================================================


%% --------------------------------------------------------------------
%% External functions
%% --------------------------------------------------------------------


%% --------------------------------------------------------------------
%% @doc
%% Format debug printouts.
%%
%% The Tag parameter can be any erlang term. The String parameter must be a valid string.
%%
%% @end
%% --------------------------------------------------------------------
-spec print_debug(Module, Func, Tag, String) -> Result when
    Module  ::  module(),
    Func    ::  atom(),
    Tag     ::  any(),
    String::string(),
    Result ::  ok.
print_debug(Module, Func, Tag, String) ->
    % Bypass EUnit and print text directly to the console while testing,
    %io:format(user, io:format("~nDEBUG: ~p ~p:~p ~p:~p ~p~n~s", [self(), calendar:now_to_local_time({A,B,C}), C, Module, Func, Tag, String]),

    {A,B,C} = erlang:timestamp(),
    case String of
        [] ->
            ok = io:format("~nDEBUG: ~p ~p:~p ~p:~p ~w~n",
                [self(), calendar:now_to_local_time({A,B,C}), C, Module, Func, Tag]);
        _ ->
            ok = io:format("~nDEBUG: ~p ~p:~p ~p:~p ~w~n~s",
                [self(), calendar:now_to_local_time({A,B,C}), C, Module, Func, Tag, String])
    end.


%% --------------------------------------------------------------------
%% @doc
%% Starts the application. Can for example be called by Yaws (indirectly through someappxyzYaws) or with erl -run M F.
%% @end
%% --------------------------------------------------------------------
-spec start_app() -> ok.
start_app() ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE),
    ok = application:start(?APP_TAG),
    ?DBG(?MODULE, ?FUNCTION_NAME, ?APP_TAG),
    ok.



%% --------------------------------------------------------------------
%% @doc
%% Stops the application.
%% @end
%% --------------------------------------------------------------------
-spec stop_app() -> ok.
stop_app() ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE),
    ok = application:stop(?APP_TAG).


%% --------------------------------------------------------------------
%% @doc
%% Callback from the application master when the application is to be started.
%%
%% Should start the top level supervisor.
%% @end
%% --------------------------------------------------------------------
-spec start(Type, StartArgs) -> Result when
    Type        ::  normal | {takeover, Node::node()} | {failover, Node::node()},
    StartArgs   ::  any(),
    Result      ::  {ok, Pid::pid()} | {ok, Pid::pid(), State::any()} | {error, Reason::any()}.
start(Type, StartArgs) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("Type: ~p StartArgs: ~p~n", [Type, StartArgs])),
    case start_supervisor_link(StartArgs) of
        {ok, Pid} ->
            {ok, Pid, {Type, StartArgs}};  % Set "State" to  {Type, StartArgs}. Could be any term.
        Error ->
            Error
    end.

%% --------------------------------------------------------------------
%% @doc
%% Callback from the application master when the application is stopped.
%%
%% Any additional resources reserved by the application should be released here.
%% @end
%% --------------------------------------------------------------------
-spec stop(State::any()) -> ok.
stop(State) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("Application stopped! State: ~p~n", [State])),
    ok.



%% --------------------------------------------------------------------
%% @doc
%% Callback from gen_server process someappxyzGenserver when the application is starting up.
%%
%% Initialize any resources needed by the application here.
%% @end
%% --------------------------------------------------------------------
-spec app_init(StartArgs::any()) -> ok.
app_init(StartArgs)->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("Application started! StartArgs: ~p~n", [StartArgs])),

    % observer:start(),

    case application:get_env(?APP_TAG, ?MNESIA_DB) of
        {ok, {?TRUE, MnesiaDbPath}} ->
            "" = os:cmd("mkdir -p " ++ MnesiaDbPath),
            application:set_env(mnesia, dir, MnesiaDbPath ++ "/" ++ atom_to_list(node())),
            init_mnesia();
        _ ->
            ignore
    end,

    % Create dets-table(s) according to settings in the app-file.
    case application:get_env(?APP_TAG, ?DETS_TBL) of
        {ok, {?TRUE, DetsName, DetsFilePath, DetsFileName}} ->
            % Create an example dets table which can hold records; the first field in the record is the key (keypos = 2)
            DetsFilePathNode = DetsFilePath ++ "/" ++ atom_to_list(node()),
            "" = os:cmd("mkdir -p " ++ DetsFilePathNode),
            {ok, _} = dets:open_file(DetsName, [{type, set}, {keypos, 2}, {file, DetsFilePathNode ++ "/" ++ DetsFileName}]);
        _ ->
            ignore
    end,

    % Create an example ets table which can hold records; the second field is the key (keypos = 2)
	ets:new(someappxyz_ets_tbl, [set, public, named_table, {keypos,2}]),

	% Create a bunch of lightweight processes
	{ok, Pid1} = start_new_proc({1, some_data}),
	{ok, Pid2} = start_new_proc({2, some_data}),
	{ok, Pid3} = start_new_proc({3, some_data}, 1000),
	?TRUE = erlang:register(someappxyz_pid1, Pid1),
	?TRUE = erlang:register(someappxyz_pid2, Pid2),
	?TRUE = erlang:register(someappxyz_pid3, Pid3),

    happy_testing(),

    ok.


%% --------------------------------------------------------------------
%% @doc
%% Callback from gen_server process someappxyzGenserver when the application should terminate.
%%
%% Release any resources reserved by the application.
%% @end
%% --------------------------------------------------------------------
-spec app_terminate() -> ok.
app_terminate()->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("Application terminating!~n", [])),

    % Make sure that all mnesia transactions are written to disc.
    ok = mnesia:sync_log(),

    % Close any open dets tables.
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("Dets tables: ~p~n", [dets:all()])),
    close_dets_tables(dets:all()),

    % Ets tables will automatically be closed when the gen_server process dies.
    ok.



%% --------------------------------------------------------------------
%%% Internal functions
%% --------------------------------------------------------------------



%% --------------------------------------------------------------------
%% Close all dets tables
%% --------------------------------------------------------------------
close_dets_tables([]) ->
    ok;
close_dets_tables([H|T]) ->
    ok = dets:close(H),
    close_dets_tables(T).


%% --------------------------------------------------------------------
%% Create mnesia database and tables
%% --------------------------------------------------------------------
init_mnesia() ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE),
   % Create Mnesia tables
    case mnesia:create_schema([node()]) of % Create mnesia database on this node only
        ok ->
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE),
            ok;
        {error, {_, {already_exists, _}}} -> % Database already created
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE),
            ok;
        What ->
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("What: ~p~n", [What])),
            exit(What) % Crash hard
    end,
    ok = application:ensure_started(mnesia),

    % Create table (possibly to an existing mnesia database)
    case catch mnesia:table_info(?SOMEAPPXYZ_MNESIA_TABLE, type) of
        set ->
            ok; % Already exists
        _ ->
            {atomic, ok} = mnesia:create_table(?SOMEAPPXYZ_MNESIA_TABLE,
                                                [{type, set},
                                                {attributes, record_info(fields, ?SOMEAPPXYZ_MNESIA_TABLE)},
                                                {record_name,  ?SOMEAPPXYZ_MNESIA_TABLE},
                                                {disc_copies, [node()]},
                                                {access_mode, read_write}])
    end,

    % Add another table (possibly to an existing mnesia database)
    case catch mnesia:table_info(?SOMEAPPXYZ_MNESIA_OTHER_TABLE, type) of
        set ->
            ok; % Already exists
        _ ->
            {atomic, ok} = mnesia:create_table(?SOMEAPPXYZ_MNESIA_OTHER_TABLE,
                                        [{type, set},
                                        {attributes, record_info(fields, ?SOMEAPPXYZ_MNESIA_OTHER_TABLE)},
                                        {record_name,  ?SOMEAPPXYZ_MNESIA_OTHER_TABLE},
                                        {disc_copies, [node()]},
                                        {access_mode, read_write}])
    end,

    ok = mnesia:wait_for_tables([?SOMEAPPXYZ_MNESIA_TABLE, ?SOMEAPPXYZ_MNESIA_OTHER_TABLE], infinity),

  	mnesia:info().


%% --------------------------------------------------------------------
%%% Test functions
%% --------------------------------------------------------------------



%%% Eunit tests
debug_test_() ->
    [?_test(?assert(print_debug(?MODULE, ?FUNCTION_NAME, ?LINE, "") =:= ok)),
    ?_test(?assert(print_debug(?MODULE, ?FUNCTION_NAME, {some, tag, []}, "Some extra info") =:= ok)),
    ?_test(?assert(print_debug(?MODULE, ?FUNCTION_NAME, {another, tag}, "Some extra info") =:= ok))].





