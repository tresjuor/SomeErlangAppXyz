%% ====================================================================
%%
%% Description: Callbacks from Yaws
%%
%%
%% License: MIT
%%
%% ====================================================================
% %
% % Copyright (c) 2022 Magnus Uppman <magnus.uppman@tresjuor.se>
% %
% % Permission is hereby granted, free of charge, to any person obtaining a copy
% % of this software and associated documentation files (the "Software"), to deal
% % in the Software without restriction, including without limitation the rights
% % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% % copies of the Software, and to permit persons to whom the Software is
% % furnished to do so, subject to the following conditions:
% %
% % The above copyright notice and this permission notice shall be included in all
% % copies or substantial portions of the Software.
% %
% % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% % SOFTWARE.

%% ====================================================================
%% Module
%% ====================================================================

-module(someappxyzYaws).

%% ====================================================================
%% Behaviours
%% ====================================================================

%% ====================================================================
%% Include files
%% ====================================================================
-include("someappxyz.hrl").

-include_lib("eunit/include/eunit.hrl").
-include_lib("yaws/include/yaws_api.hrl").


%% ====================================================================
%% Internal macros
%% ====================================================================

-define(DATE_PATTERN, <<_,_,_,_,$-,_,_,$-,_,_,32,_,_,$:,_,_,$:,_,_>>).

%% ====================================================================
%% Internal records
%% ====================================================================

%% ====================================================================
%% Function declarations
%% ====================================================================

%% --------------------------------------------------------------------
%% External exported functions
%% --------------------------------------------------------------------

-export([start/0]).

-export([out/1]).

%% --------------------------------------------------------------------
%% Internal exported functions
%% --------------------------------------------------------------------

-export([out_int/1]).

%% --------------------------------------------------------------------
%% Imported functions
%% --------------------------------------------------------------------
-import(someappxyzMain, [print_debug/4, start_app/0]).

% Imported test functions
-import(someappxyzTest, [write_dets/2, read_dets/1, write_ets/2, delete_dets/1, read_ets/1, delete_ets/1, write_mnesia/2, read_mnesia/1, dirty_read_mnesia/1]).
-import(someappxyzTest, [write_dets_int/2, read_dets_int/1, write_ets_int/2, delete_dets_int/1, read_ets_int/1, delete_ets_int/1,
            write_mnesia_int/2, read_mnesia_int/1, dirty_read_mnesia_int/1]).



%% ====================================================================
%% Function definitions
%% ====================================================================


%% --------------------------------------------------------------------
%% External functions
%% --------------------------------------------------------------------



%% --------------------------------------------------------------------
%% @doc
%% Callback from Yaws when a web request should be handled
%% @end
%% --------------------------------------------------------------------
-spec out(A) -> Result when
    A  ::  tuple(),
    Result :: any().
out(A) ->
  case catch apply(?MODULE, out_int, [A]) of
    {'EXIT', Reason} -> % Handle any crashes here to avoid that any internal data (in a crash report) is returned by Yaws.
      ?DBG(?MODULE,  ?FUNCTION_NAME,  ?LINE, io_lib:format("Crash!~nA: ~tp~nReason: ~tp~n", [A, Reason])),
      {status, 401};
    Res ->
      Res
  end.


%% --------------------------------------------------------------------
%% Interal exported functions
%% --------------------------------------------------------------------

out_int(A) ->  %% Runs in yaws process
    Host = (A#arg.headers)#headers.host,
    Metod = (A#arg.req)#http_request.method,
    {abs_path, Path} = (A#arg.req)#http_request.path,
    ?DBG(?MODULE,  ?FUNCTION_NAME,  ?LINE, io_lib:format("A: ~tp~n", [A])),
    ?DBG(?MODULE,  ?FUNCTION_NAME,  ?LINE, io_lib:format("Host: ~p Path: ~p~n", [Host, Path])),
    {RestPath, Params} = get_rest_info(Path),
    TimeStamp = erlang:timestamp(),
    T1 = calendar:now_to_local_time(TimeStamp),
    T2 = calendar:now_to_universal_time(TimeStamp),
    case {Metod, RestPath} of
        {'GET', time} ->
            JsonReplyObj =
            {obj, [
                {"utc_time", str_to_bin(datetime_to_string(T2))},
                {"server_localtime", str_to_bin(datetime_to_string(T1))}
                ]},
            JsonReplyStr = rfc4627:encode(JsonReplyObj),
            [{status, 200}, {content, "application/json; charset=utf-8",  JsonReplyStr}];
        {'POST', simple_db} ->
            {ok, Json, Undecoded} = rfc4627:decode(A#arg.clidata),
            ?DBG(?MODULE,  ?FUNCTION_NAME,  ?LINE, io_lib:format("POST request - Json: ~tp Undecoded: ~tp~n", [Json, Undecoded])),
            Key = rfc4627:get_field(Json, "key", <<>>),
            Data = rfc4627:get_field(Json, "data", <<>>),
            case read_ets(Key) of
                {ok, _} ->
                    {status, 400};
                _ ->
                    JsonReplyObj =
                    {obj, [
                        {"key", Key},
                        {"server_localtime", str_to_bin(datetime_to_string(T1))}
                        ]},
                    write_ets(Key, {Data, T1}),
                    JsonReplyStr = rfc4627:encode(JsonReplyObj),
                    [{status, 201}, {content, "application/json; charset=utf-8",  JsonReplyStr}]
            end;
        {'PUT', simple_db_object} ->
            {ok, Json, Undecoded} = rfc4627:decode(A#arg.clidata),
            ?DBG(?MODULE,  ?FUNCTION_NAME,  ?LINE, io_lib:format("PUT request - Json: ~tp Undecoded: ~tp~n", [Json, Undecoded])),
            Key = str_to_bin(Params),
            Data = rfc4627:get_field(Json, "data", <<>>),
            case read_ets(Key) of
                {ok, _} ->
                    write_ets(Key, {Data, T1}),
                    JsonReplyObj =
                    {obj, [
                        {"key", Key},
                        {"server_localtime", str_to_bin(datetime_to_string(T1))}
                        ]},
                    JsonReplyStr = rfc4627:encode(JsonReplyObj),
                    [{status, 200}, {content, "application/json; charset=utf-8",  JsonReplyStr}];
                _ ->
                    {status, 400}
            end;
        {'GET', simple_db_object} ->
            Key = str_to_bin(Params),
            case read_ets(Key) of
                {ok, Rec} ->
                    {ReadData, ReadTimeStamp} = Rec#some_ets_record.data1,
                    JsonReplyObj =
                    {obj, [
                        {"key", Key},
                        {"data", ReadData},
                        {"server_localtime", str_to_bin(datetime_to_string(ReadTimeStamp))}
                        ]},
                    JsonReplyStr = rfc4627:encode(JsonReplyObj),
                    [{status, 200}, {content, "application/json; charset=utf-8",  JsonReplyStr}];
                _ ->
                   {status, 410}
            end;
        {'DELETE', simple_db_object} ->
            Key = str_to_bin(Params),
            delete_ets(Key),
            JsonReplyObj =
            {obj, [
                {"key", Key}
                ]},
            JsonReplyStr = rfc4627:encode(JsonReplyObj),
            [{status, 200}, {content, "application/json; charset=utf-8",  JsonReplyStr}];
        What ->
            ?DBG(?MODULE,  ?FUNCTION_NAME,  ?LINE, io_lib:format("What: ~tp~n", [What])),
            {status, 401}
    end.



%% --------------------------------------------------------------------
%% @doc
%% Starts the application from Yaws.
%% @end
%% --------------------------------------------------------------------
-spec start() -> ok.
start() ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE),
    start_app().


%% --------------------------------------------------------------------
%%% Internal functions
%% --------------------------------------------------------------------


% Convert UTF-8 string to UTF-8 binary
str_to_bin(Str) ->
 unicode:characters_to_binary(Str, utf8, utf8).

% Decode REST paths
get_rest_info([$/, $s, $i, $m, $p, $l, $e, $_, $d, $b, $/|T]) ->
    {simple_db_object, T};
get_rest_info([$/, $s, $i, $m, $p, $l, $e, $_, $d, $b|T]) ->
    {simple_db, T};
get_rest_info([$/, $t, $i, $m, $e]) ->
    {time, []};
get_rest_info(_) ->
    {?UNDEFINED, []}.


% datetime_to_string/1
datetime_to_string(undefined) ->
  "";
datetime_to_string({Year, Month, Day}) ->
  integer_to_list(Year) ++ "-" ++ pad_with_zero(integer_to_list(Month)) ++ "-" ++ pad_with_zero(integer_to_list(Day));
datetime_to_string(DateTime) ->
   {{Year, Month, Day}, {Hour, Min, Sec}} = DateTime,
   integer_to_list(Year) ++  "-" ++  pad_with_zero(integer_to_list(Month)) ++ "-" ++ pad_with_zero(integer_to_list(Day))
    ++ " " ++ pad_with_zero(integer_to_list(Hour)) ++ ":" ++  pad_with_zero(integer_to_list(Min)) ++ ":" ++ pad_with_zero(integer_to_list(Sec)).

%  pad_with_zero/1
pad_with_zero([A,B]) ->
  [A,B];
pad_with_zero([A]) ->
  [$0,A].

%% --------------------------------------------------------------------
%%% Test functions
%% --------------------------------------------------------------------


%%% Eunit tests
debug_test_() ->
    [?_test(?assert(time_test_REST() =:= ok)),
    ?_test(?assert(time_test_fail_REST() =:= ok)),
    ?_test(?assert(db_test_1_REST() =:= ok)),
    ?_test(?assert(db_test_2_REST() =:= ok)),
    ?_test(?assert(db_test_3_REST() =:= ok)),
    ?_test(?assert(db_test_4_REST() =:= ok)),
    ?_test(?assert(db_test_5_REST() =:= ok)),
    ?_test(?assert(db_test_6_REST() =:= ok)),
    ?_test(?assert(db_test_7_REST() =:= ok)),
    ?_test(?assert(db_test_8_REST() =:= ok))].

% Test time server
time_test_REST() ->
    Res = os:cmd("curl -s -k -b cookiejar  https://localhost:9000/time"),
    {ok, {obj,[{"utc_time", ?DATE_PATTERN},
        {"server_localtime", ?DATE_PATTERN}]}, []} = rfc4627:decode(Res),
    ok.

% Test time server with faulty URL
time_test_fail_REST() ->
    Res = os:cmd("curl -s -k -b cookiejar -i https://localhost:9000/timebvb"),
    [_|_] = string:prefix(Res, "HTTP/1.1 401 Unauthorized"),
    ok.

% Test add entry to database
db_test_1_REST() ->
    Res = os:cmd("curl -s -k -b cookiejar -H \"Content-Type: application/json\" -X POST -d '{\"key\":\"2\", \"data\":\"some data 123456789\"}' https://localhost:9000/simple_db"),
    {ok, {obj,[{"key", <<"2">>},
       {"server_localtime", ?DATE_PATTERN}]}, []} = rfc4627:decode(Res),
    ok.

% Test add duplicate entry to database
db_test_2_REST() ->
    Res = os:cmd("curl -s -k -b cookiejar -i -H \"Content-Type: application/json\" -X POST -d '{\"key\":\"2\", \"data\":\"some data 123456789\"}' https://localhost:9000/simple_db"),
    [_|_] = string:prefix(Res, "HTTP/1.1 400 Bad Request"),
    ok.

% Test read entry from database
db_test_3_REST() ->
    Res = os:cmd("curl -s -k -b cookiejar -X GET https://localhost:9000/simple_db/2"),
    {ok, {obj,[{"key", <<"2">>},
        {"data", <<"some data 123456789">>},
        {"server_localtime", ?DATE_PATTERN}]}, []} = rfc4627:decode(Res),
    ok.


% Test update entry in database
db_test_4_REST() ->
    Res = os:cmd("curl -s -k -b cookiejar -H \"Content-Type: application/json\" -X PUT -d '{\"data\":\"some new data 999\"}' https://localhost:9000/simple_db/2"),
    {ok, {obj,[{"key", <<"2">>},
       {"server_localtime", ?DATE_PATTERN}]}, []} = rfc4627:decode(Res),
    ok.


% Test read updated entry from database
db_test_5_REST() ->
    Res = os:cmd("curl -s -k -b cookiejar -X GET https://localhost:9000/simple_db/2"),
    {ok, {obj,[{"key", <<"2">>},
        {"data", <<"some new data 999">>},
        {"server_localtime", ?DATE_PATTERN}]}, []} = rfc4627:decode(Res),
    ok.


% Test delete entry from database
db_test_6_REST() ->
    Res = os:cmd("curl -s -k -b cookiejar -i -X DELETE https://localhost:9000/simple_db/2"),
    [_|_] = string:prefix(Res, "HTTP/1.1 200 OK"),
    ok.


% Test update deleted entry in database
db_test_7_REST() ->
    Res = os:cmd("curl -s -k -b cookiejar -i -H \"Content-Type: application/json\" -X PUT -d '{\"data\":\"some new data 999\"}' https://localhost:9000/simple_db/2"),
    [_|_] = string:prefix(Res, "HTTP/1.1 400 Bad Request"),
    ok.


% Test read deleted entry in database
db_test_8_REST() ->
    Res = os:cmd("curl -s -k -b cookiejar -i -X GET https://localhost:9000/simple_db/2"),
    [_|_] = string:prefix(Res, "HTTP/1.1 410 Gone"),
    ok.

