%% ====================================================================
%%
%% Description: Example gen_server for the application
%%
%%
%% License: MIT
%%
%% ====================================================================
% %
% % Copyright (c) 2022 Magnus Uppman <magnus.uppman@tresjuor.se>
% %
% % Permission is hereby granted, free of charge, to any person obtaining a copy
% % of this software and associated documentation files (the "Software"), to deal
% % in the Software without restriction, including without limitation the rights
% % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% % copies of the Software, and to permit persons to whom the Software is
% % furnished to do so, subject to the following conditions:
% %
% % The above copyright notice and this permission notice shall be included in all
% % copies or substantial portions of the Software.
% %
% % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% % SOFTWARE.

%% ====================================================================
%% Module
%% ====================================================================

-module(someappxyzGenserver).

%% ====================================================================
%% Behaviours
%% ====================================================================

-behaviour(gen_server).

%% ====================================================================
%% Include files
%% ====================================================================

-include("someappxyz.hrl").


%% ====================================================================
%% Internal macros
%% ====================================================================

-define(TIMEOUT, 1000).   % Enable a timeout which is handled in handle_info(timeout, State) every X ms
%-define(TIMEOUT, infinity). % Disable the timeout

%% ====================================================================
%% Internal records
%% ====================================================================

% Loopdata
-record(state, {}).

%% ====================================================================
%% Function declarations
%% ====================================================================

%% --------------------------------------------------------------------
%% External exported functions
%% --------------------------------------------------------------------

% Behavioural exports
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

% API exports
-export([start_link/1, stop/0, call/3, call/4, cast/3]).


%% --------------------------------------------------------------------
%% Internal exported functions
%% --------------------------------------------------------------------

%% --------------------------------------------------------------------
%% Imported functions
%% --------------------------------------------------------------------

-import(someappxyzMain, [print_debug/4]).

-import(someappxyzMain, [app_init/1, app_terminate/0]).

%% ====================================================================
%% Function definitions
%% ====================================================================


%% --------------------------------------------------------------------
%% External functions
%% --------------------------------------------------------------------

%% --------------------------------------------------------------------
%% @doc
%% Callback from the supervisor, starts the gen_server process.
%%
%% No initialization should be done here.
%% @end
%% --------------------------------------------------------------------
-spec start_link(StartArgs) -> Result when
    StartArgs   ::  any(),
    Result      ::  {ok, pid()} | ignore | {error, {already_started, pid()} | any()}.
start_link(StartArgs)->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("~p~n", [StartArgs])),
    % Start a gen_server and register the process locally with the same name as the module.
    gen_server:start_link({local, ?MODULE}, ?MODULE, StartArgs, []).


%% --------------------------------------------------------------------
%% @doc
%% Manually stop the gen_server.
%% @end
%% --------------------------------------------------------------------
-spec stop() -> ok.
stop() ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE),
    gen_server:stop(?MODULE, kill, infinity).


%% --------------------------------------------------------------------
%% @doc
%% Make a synchronous call to the gen_server without specifying a timeout. Will default to infinity.
%% @end
%% --------------------------------------------------------------------
-spec call(M, F, A) -> any() when
    M   ::  module(),
    F   ::  atom(),
    A   ::  [any()].
call(M, F, A) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE),
    call(M, F, A, infinity).


%% --------------------------------------------------------------------
%% @doc
%% Make a synchronous call to the gen_server with a specific timeout value.
%% @end
%% --------------------------------------------------------------------
-spec call(M, F, A, Timeout) -> any() when
    M       ::  module(),
    F       ::  atom(),
    A       ::  [any()],
    Timeout ::  pos_integer() | infinity.
call(M, F, A, Timeout) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE),
    Self = self(),
    case whereis(?MODULE) of
       Self -> % Avoid deadlocks if called by itself
            apply(M, F, A);
        _ ->
            gen_server:call(?MODULE, {mfa, {M, F, A}}, Timeout)
    end.



%% --------------------------------------------------------------------
%% @doc
%% Make an asynchronous call to the gen_server.
%% @end
%% --------------------------------------------------------------------
-spec cast(M, F, A) -> ok when
    M   ::  module(),
    F   ::  atom(),
    A   ::  [any()].
cast(M, F, A) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE),
    gen_server:cast(?MODULE, {mfa, {M, F, A}}).


%% --------------------------------------------------------------------
%% @doc
%% Initiates the gen_server
%% @end
%% --------------------------------------------------------------------
-spec init(StartArgs) -> Result when
    StartArgs   ::  any(),
    Result      ::  {ok,State} |
                    {ok,State,Timeout} |
                    {ok,State,hibernate} |
                    {ok,State,{continue,Continue}} |
                    {stop,Reason} |
                    ignore,
    Timeout     ::  non_neg_integer() | infinity,
    Continue    ::  any(),
    Reason      ::  any().
init(StartArgs) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("~p~n", [StartArgs])),
    process_flag(trap_exit, true),  % Trap exits so that terminate will be called if the supervisor shuts the process down (for whatever reason).
    app_init(StartArgs), % application specific initialization
    {ok, #state{}, ?TIMEOUT}. % Setup a timer which triggers every ?TIMEOUT ms.

 
%% --------------------------------------------------------------------
%% @doc
%% Handle call messages
%% @end
%% --------------------------------------------------------------------
-spec handle_call(Request, From, State) -> Result when
    Request ::  term(),
    From    ::  {pid(), Tag::any()},
    State   ::  term(),
    Result  ::  {reply,Reply,NewState} | {reply,Reply,NewState,Timeout}
                | {reply,Reply,NewState,hibernate}
                | {reply,Reply,NewState,{continue,Continue}}
                | {noreply,NewState} | {noreply,NewState,Timeout}
                | {noreply,NewState,hibernate}
                | {noreply,NewState,{continue,Continue}}
                | {stop,Reason,Reply,NewState} | {stop,Reason,NewState},
    Reply       :: term(),
    NewState    :: term(),
    Timeout     :: non_neg_integer() | infinity,
    Continue    :: term(),
    Reason      :: term().
handle_call({mfa, {M, F, A}}, From, State) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("From: ~w~n~w~n", [From, {mfa, {M, F, A}}])),
    Reply = apply(M, F, A),
    {reply, Reply, State, ?TIMEOUT};
handle_call(Request, From, State) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("~w ~w ~w~n", [Request, From, State])),
    Reply = ok,
    {reply, Reply, State, ?TIMEOUT}.
  
  
%% --------------------------------------------------------------------
%% @doc
%% Handle cast messages
%% @end
%% --------------------------------------------------------------------
-spec handle_cast(Request, State) -> Result when
    Request ::  term(),
    State   ::  term(),
    Result  ::  {noreply,NewState} | {noreply,NewState,Timeout}
                | {noreply,NewState,hibernate}
                | {noreply,NewState,{continue,Continue}}
                | {stop,Reason,NewState},
    NewState    :: term(),
    Timeout     :: non_neg_integer() | infinity,
    Continue    :: term(),
    Reason      :: term().
handle_cast({mfa, {M, F, A}}, State) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("~w~n", [{mfa, {M, F, A}}])),
    apply(M, F, A),
    {noreply, State, ?TIMEOUT};
handle_cast(Msg, State) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("Msg: ~w~n", [Msg])),
    {noreply, State, ?TIMEOUT}.



	    
%% --------------------------------------------------------------------
%% @doc
%% Handling all non call/cast messages
%% @end
%% --------------------------------------------------------------------
-spec handle_info(Info, State) -> Result when
    Info    ::  timeout | term(),
    State   ::  term(),
    Result  ::  {noreply,NewState} | {noreply,NewState,Timeout}
                | {noreply,NewState,hibernate}
                | {noreply,NewState,{continue,Continue}}
                | {stop,Reason,NewState},
    NewState    :: term(),
    Timeout     :: non_neg_integer() | infinity,
    Continue    :: term(),
    Reason      :: term().
handle_info({'EXIT', Pid, Reason}, State) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("~p~n", [{'EXIT', Pid, Reason}])),
    {noreply, State, ?TIMEOUT};
handle_info(timeout, State) ->
    %?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("timeout State: ~p~n", [State])),
    {noreply, State, ?TIMEOUT};
handle_info(_Info, State) ->
    %?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("Info: ~p State: ~p~n", [Info, State])),
    {noreply, State,  ?TIMEOUT}.

%% --------------------------------------------------------------------
%% @doc
%% Called when the gen_server is shutting down.
%% @end
%% --------------------------------------------------------------------
-spec terminate(Reason, State) -> any() when
    Reason  ::  normal | shutdown | {shutdown,term()} | term(),
    State   ::  term().
terminate(Reason, State) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("Reason: ~p State: ~p~n", [Reason, State])),
    app_terminate().

%% --------------------------------------------------------------------
%% @doc
%% Convert process state when code is changed
%% @end
%% --------------------------------------------------------------------
-spec code_change(OldVsn, State, Extra) -> Result when
    OldVsn  ::  Vsn | {down, Vsn},
    Vsn     ::  term(),
    State   ::  term(),
    Result  ::  {ok, NewState} | {error, Reason},
    NewState    ::  term(),
    Extra       ::  term(),
    Reason      ::  term().
code_change(OldVsn, State, Extra) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("OldVsn: ~p State: ~p Extra: ~p~n", [OldVsn, State, Extra])),
    {ok, State}.

%% --------------------------------------------------------------------
%%% Internal functions
%% --------------------------------------------------------------------
