%% ====================================================================
%%
%% Description: Top level supervisor for the application
%%
%%
%% License: MIT
%%
%% ====================================================================
% %
% % Copyright (c) 2022 Magnus Uppman <magnus.uppman@tresjuor.se>
% %
% % Permission is hereby granted, free of charge, to any person obtaining a copy
% % of this software and associated documentation files (the "Software"), to deal
% % in the Software without restriction, including without limitation the rights
% % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% % copies of the Software, and to permit persons to whom the Software is
% % furnished to do so, subject to the following conditions:
% %
% % The above copyright notice and this permission notice shall be included in all
% % copies or substantial portions of the Software.
% %
% % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% % SOFTWARE.


%% ====================================================================
%% Module
%% ====================================================================

-module(someappxyzSupervisor).

%% ====================================================================
%% Behaviours
%% ====================================================================

-behaviour(supervisor).


%% ====================================================================
%% Include files
%% ====================================================================
-include("someappxyz.hrl").

%% ====================================================================
%% Internal macros
%% ====================================================================

%% ====================================================================
%% Internal records
%% ====================================================================

%% ====================================================================
%% Function declarations
%% ====================================================================

%% --------------------------------------------------------------------
%% External exported functions
%% --------------------------------------------------------------------

-export([start_supervisor_link/1]).


%% --------------------------------------------------------------------
%% Internal exported functions
%% --------------------------------------------------------------------

-export([
         init/1
        ]).


%% --------------------------------------------------------------------
%% Imported functions
%% --------------------------------------------------------------------

-import(someappxyzMain, [print_debug/4]).

%% ====================================================================
%% Function definitions
%% ====================================================================

%% --------------------------------------------------------------------
%% External functions
%% --------------------------------------------------------------------

%% --------------------------------------------------------------------
%% @doc
%% Starts the top level supervisor for the application
%% @end
%% --------------------------------------------------------------------
-spec start_supervisor_link(StartArgs) -> Result when
    StartArgs   ::  any(),
    Result      ::  {ok, pid()} | {error, {already_started, pid()} | {shutdown, term()} | term()}.
start_supervisor_link(StartArgs)->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("StartArgs: ~p~n", [StartArgs])),
    supervisor:start_link({local, ?MODULE}, ?MODULE, StartArgs).

%

%% --------------------------------------------------------------------
%% @doc
%% Set supervisor restart strategy, maximum restart intensity, and child specifications.
%% @end
%% --------------------------------------------------------------------
-spec init(StartArgs) -> Result when
    StartArgs   ::  any(),
    Result      ::  {ok,  {SupFlags, [ChildSpec]}} | ignore,
    SupFlags :: supervisor:sup_flags(),
    ChildSpec :: supervisor:child_spec().
init(StartArgs) ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("~p~n", [StartArgs])),
    SupFlags = #{strategy => one_for_one, % Only restart the worker that has crashed (one_for_one).
                intensity => 2,   % Allow maximum two restarts
                period => 60,     % in 60s
                auto_shutdown => never},
    ChildSpec = #{id => someappxyzGenserver,  % Tag
                    start => {someappxyzGenserver, start_link, [StartArgs]}, % Start MFA
                    restart => permanent,  % Always restart
                    significant => false,
                    shutdown => 20000,  % Maximum shutdown time in ms before the worker is killed
                    type => worker,    % Type of process. Used at software upgrade
                    modules => [someappxyzGenserver]},  % Module list. Used at software upgrade
    {ok, {SupFlags, [ChildSpec]}}.



%% --------------------------------------------------------------------
%%% Internal functions
%% --------------------------------------------------------------------

