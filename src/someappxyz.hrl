%% ====================================================================
%%
%% Description: Include file for the application
%%
%%
%% License: MIT
%%
%% ====================================================================
% %
% % Copyright (c) 2022 Magnus Uppman <magnus.uppman@tresjuor.se>
% %
% % Permission is hereby granted, free of charge, to any person obtaining a copy
% % of this software and associated documentation files (the "Software"), to deal
% % in the Software without restriction, including without limitation the rights
% % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% % copies of the Software, and to permit persons to whom the Software is
% % furnished to do so, subject to the following conditions:
% %
% % The above copyright notice and this permission notice shall be included in all
% % copies or substantial portions of the Software.
% %
% % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% % SOFTWARE.

%% ====================================================================
%% Macros
%% ====================================================================

% Uncomment DEBUG to create debug compiled code.
-define(DEBUG, true).

-ifdef(DEBUG).
    -define(DBG(V1, V2, V3), print_debug(V1, V2, V3, "")).
    -define(DBG(V1, V2, V3, V4), print_debug(V1, V2, V3, V4)).
-else.
    -define(DBG(V1, V2, V3), true).
    -define(DBG(V1, V2, V3, V4), true).
-endif.

% Uncomment NOTEST to disable Eunit tests
% -define(NOTEST, 1).

-define(APP_TAG, someappxyz).

-define(TRUE, true).
-define(FALSE, false).

-define(UNDEFINED, undefined).

% Environment variables in the app-file

% Enable/disable dets. Format in app-file: {dets_tbl, {true/false, DetsName, DetsFilePath, DetsFileName}}
-define(DETS_TBL, dets_tbl).

% Enable/disable mnesia. Format in app-file: {mnesia_db, {true/false, MnesiaDbPath}}
-define(MNESIA_DB, mnesia_db).

% Mnesia tables
-define(SOMEAPPXYZ_MNESIA_TABLE, 'SOMEAPPXYZ_MNESIA_TABLE').
-define(SOMEAPPXYZ_MNESIA_OTHER_TABLE, 'SOMEAPPXYZ_MNESIA_OTHER_TABLE').

%% ====================================================================
%% Records
%% ====================================================================

% Record to be stored in the example dets table.
% It is recommended to only store one type of records in a dets table.
-record(some_record,
{key,  %
data1, %
data2, %
data3, %
data4
}).

% Record to be stored in the example ets table.
% It is recommended to only store one type of records in a ets table.
-record(some_ets_record,
{key,  %
data1, %
data2
}).

% Record to be stored in the example mnesia table.
% Only one type of record is allowed in a mnesia table. The key is always the first field in the record.
-record(?SOMEAPPXYZ_MNESIA_TABLE,
{key,  %
data1, %
data2, %
data3 = 0,
data4 = 1,
data5 = x,
data6 = []
}).

% Record to be stored in another example mnesia table.
% Only one type of record is allowed in a mnesia table. The key is always the first field in the record.
-record(?SOMEAPPXYZ_MNESIA_OTHER_TABLE,
{key,  %
data1, %
data2, %
data3 = 0,
data4 = 1,
data5 = x,
data6 = []
}).
