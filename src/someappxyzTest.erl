%% ====================================================================
%%
%% Description: Entry point for running tests
%%
%%
%% License: MIT
%%
%% ====================================================================
% %
% % Copyright (c) 2022 Magnus Uppman <magnus.uppman@tresjuor.se>
% %
% % Permission is hereby granted, free of charge, to any person obtaining a copy
% % of this software and associated documentation files (the "Software"), to deal
% % in the Software without restriction, including without limitation the rights
% % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% % copies of the Software, and to permit persons to whom the Software is
% % furnished to do so, subject to the following conditions:
% %
% % The above copyright notice and this permission notice shall be included in all
% % copies or substantial portions of the Software.
% %
% % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% % SOFTWARE.


%% ====================================================================
%% Module
%% ====================================================================

-module(someappxyzTest).

%% ====================================================================
%% Behaviours
%% ====================================================================


%% ====================================================================
%% Include files
%% ====================================================================
-include("someappxyz.hrl").

%% ====================================================================
%% Internal macros
%% ====================================================================

%% ====================================================================
%% Internal records
%% ====================================================================

%% ====================================================================
%% Function declarations
%% ====================================================================

%% --------------------------------------------------------------------
%% External exported functions
%% --------------------------------------------------------------------

-export([start/0, run_tests/0, happy_testing/0]).


% Exported test functions
-export([write_dets/2, read_dets/1, write_ets/2, delete_dets/1, read_ets/1, delete_ets/1, write_mnesia/2, read_mnesia/1, dirty_read_mnesia/1]).
-export([write_dets_int/2, read_dets_int/1, write_ets_int/2, delete_dets_int/1, read_ets_int/1, delete_ets_int/1,
            write_mnesia_int/2, read_mnesia_int/1, dirty_read_mnesia_int/1]).

%% --------------------------------------------------------------------
%% Internal exported functions
%% --------------------------------------------------------------------

-export([
        ]).


%% --------------------------------------------------------------------
%% Imported functions
%% --------------------------------------------------------------------

-import(someappxyzMain, [start_app/0, stop_app/0, print_debug/4]).


% Gen_server imports
-import(someappxyzGenserver, [call/3, call/4, cast/3]).


%% ====================================================================
%% Function definitions
%% ====================================================================

%% --------------------------------------------------------------------
%% External functions
%% --------------------------------------------------------------------


%% --------------------------------------------------------------------
%% @doc
%% Starts the application from Yaws and run Eunit tests
%% @end
%% --------------------------------------------------------------------
-spec start() -> ok.
start() ->
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE),
    cover:start(),
    [_|_] = cover:compile_directory("./src"),
    start_app(),
    someappxyzMain:test(),
    timer:sleep(2000),
    someappxyzProc:test(),
    timer:sleep(2000),
    someappxyzYaws:test(),
    timer:sleep(2000),
    stop_app(),
    timer:sleep(2000),
    start_app(),
    stop_app(),
    timer:sleep(2000),
    {result, Res, []} = cover:analyse(line),
    % io:format("Coverage:~n~p~n", [Res]),
    print_uncovered_code(Res, [serial, rfc4627, someappxyzTest]),
    init:stop().




%% --------------------------------------------------------------------
%% @doc
%% Run Eunit tests (excluding Yaws specific code)
%% @end
%% --------------------------------------------------------------------
run_tests() ->
    cover:start(),
    [_|_] = cover:compile_directory("./src"),
    start_app(),
    someappxyzMain:test(),
    timer:sleep(2000),
    someappxyzProc:test(),
    timer:sleep(2000),
    stop_app(),
    timer:sleep(2000),
    start_app(),
    stop_app(),
    timer:sleep(2000),
    {result, Res, []} = cover:analyse(line),
    %io:format("Coverage:~n~p~n", [Res]),
    print_uncovered_code(Res, [serial, rfc4627, someappxyzTest, someappxyzYaws]),
    init:stop().



%% --------------------------------------------------------------------
%% @doc
%% Happy testing of the code for coverage purposes.
%% @end
%% --------------------------------------------------------------------
happy_testing() ->
    someappxyz_pid1 ! {open_serial_port, 115200, "/dev/ttyUSB0"},
    someappxyz_pid1 ! {send, "hej hopp!"},

    % Test read and write to Dets
    case application:get_env(?APP_TAG, ?DETS_TBL) of
        {ok, {?TRUE, _, _, _}} ->
            DetsKey = 1,
            delete_dets(1),
            DetsData = {[1,2,3], an_atom},
            ok = write_dets(DetsKey, DetsData),
            {ok, DetsRec} = read_dets(DetsKey),
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("DetsRec: ~p~n", [DetsRec])),
            DetsData_2 = {[4,5,6], another_atom},
            ok = write_dets(DetsKey, DetsData_2), % Overwrite existing entry
            {ok, UpdatedDetsRec} = read_dets(DetsKey),
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("UpdatedDetsRec: ~p~n", [UpdatedDetsRec]));
        _ ->
            ok
    end,

    % Test read and write to Ets
    EtsKey = 1,
    delete_ets(1),
    EtsData = {[1,2,3], an_atom},
    ok = write_ets(EtsKey, EtsData),
    {ok, EtsRec} = read_ets(EtsKey),
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("EtsRec: ~p~n", [EtsRec])),
    EtsData_2 = {[4,5,6], another_atom},
    ok = write_ets(EtsKey, EtsData_2), % Overwrite existing entry
    {ok, UpdatedEtsRec} = read_ets(EtsKey),
    ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("UpdatedEtsRec: ~p~n", [UpdatedEtsRec])),
%
%     % Test read and write to mnesia
    case application:get_env(?APP_TAG, ?MNESIA_DB) of
        {ok, {?TRUE, _}} ->
            MnesiaKey = 43,
            MnesiaData = #{a_tuple => {q,w,e,r,t,y}, an_int => 123, a_list => [j,k,l]},
            ok = write_mnesia(MnesiaKey, MnesiaData),
            {TimeRead, {ok, [MnesiaRec]}} = timer:tc(fun() -> read_mnesia(43) end),
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("TimeRead: ~p MnesiaRec: ~p~n", [TimeRead, MnesiaRec])),
            {DirtyTimeRead, {ok, [MnesiaRec]}} = timer:tc(fun() -> dirty_read_mnesia(43) end),
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("DirtyTimeRead: ~p MnesiaRec: ~p~n", [DirtyTimeRead, MnesiaRec])),

            % Read from mnesia without involving the gen_server
            {TimeRead_2, {ok, [MnesiaRec]}} = timer:tc(fun() -> read_mnesia_int(43) end),
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("TimeRead_2: ~p MnesiaRec: ~p~n", [TimeRead_2, MnesiaRec])),
            {DirtyTimeRead_2, {ok, [MnesiaRec]}} = timer:tc(fun() -> dirty_read_mnesia_int(43) end),
            ?DBG(?MODULE, ?FUNCTION_NAME, ?LINE, io_lib:format("DirtyTimeRead_2: ~p MnesiaRec: ~p~n", [DirtyTimeRead_2, MnesiaRec])),

            % Test read and write to mnesia from lightweight process
            whereis(someappxyz_pid2)!mnesia;
        _ ->
            ok
    end.



%% --------------------------------------------------------------------
%%% Internal functions
%% --------------------------------------------------------------------

print_uncovered_code([], _) ->
    ok;
print_uncovered_code([{{M, _}, {_, Cnt}} = H|T], IgnoreModules) when Cnt > 0 ->
    case lists:member(M, IgnoreModules) of
        ?TRUE ->
            ok;
        _ ->
            io:format("Uncovered code: ~p~n", [H])
    end,
    print_uncovered_code(T, IgnoreModules);
print_uncovered_code([_|T], IgnoreModules ) ->
    print_uncovered_code(T, IgnoreModules).




%% --------------------------------------------------------------------
%%% Test functions
%% --------------------------------------------------------------------


%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Delete entry in Dets table, switch process to the gen_server.
%% @end
%% --------------------------------------------------------------------
delete_dets(Key) ->
    call(?MODULE, delete_dets_int, [Key]).

%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Delete entry in Dets table.
%% @end
%% --------------------------------------------------------------------
delete_dets_int(Key) ->
    Table = someappxyz_dets_tab_1,
    dets:delete(Table, Key).

%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Write entry to Dets table, switch process to the gen_server.
%% @end
%% --------------------------------------------------------------------
write_dets(Key, Data) ->
    call(?MODULE, write_dets_int, [Key, Data]).

%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Write entry to Dets table.
%% @end
%% --------------------------------------------------------------------
write_dets_int(Key, Data) ->
    Table = someappxyz_dets_tab_1,
    NewRec =
    case dets:lookup(Table, Key) of
        [] ->
            #some_record{key = Key, data1 = Data, data2 = a, data3 = b, data4 = c};
        [Rec] ->
            Rec#some_record{data1 = Data, data2 = a, data3 = q, data4 = w}
    end,
    ok = dets:insert(Table, NewRec),
    dets:sync(Table),
    ok.

%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Read entry from Dets table, switch process to the gen_server.
%% @end
%% --------------------------------------------------------------------
read_dets(Key) ->
    call(?MODULE, read_dets_int, [Key]).

%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Read entry from Dets table.
%% @end
%% --------------------------------------------------------------------
read_dets_int(Key) ->
    Table = someappxyz_dets_tab_1,
    case dets:lookup(Table, Key) of
        [] ->
            {error, record_not_found};
        [Rec] ->
            {ok, Rec}
    end.



%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Delete entry from Ets table, switch process to the gen_server.
%% @end
%% --------------------------------------------------------------------
delete_ets(Key) ->
    call(?MODULE, delete_ets_int, [Key]).

%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Delete entry from Ets table.
%% @end
%% --------------------------------------------------------------------
delete_ets_int(Key) ->
    Table = someappxyz_ets_tbl,
    true = ets:delete(Table, Key).



%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Write entry to Ets table, switch process to the gen_server.
%% @end
%% --------------------------------------------------------------------
write_ets(Key, Data) ->
    call(?MODULE, write_ets_int, [Key, Data]).


%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Write entry to Ets table.
%% @end
%% --------------------------------------------------------------------
write_ets_int(Key, Data) ->
    Table = someappxyz_ets_tbl,
    NewRec =
    case ets:lookup(Table, Key) of
        [] ->
            #some_ets_record{key = Key, data1 = Data, data2 = whatever};
        [Rec] ->
            Rec#some_ets_record{data1 = Data, data2 = whutever}
    end,
    true = ets:insert(Table, NewRec),
    ok.


%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Read entry from Ets table, switch process to the gen_server.
%% @end
%% --------------------------------------------------------------------
read_ets(Key) ->
    call(?MODULE, read_ets_int, [Key]).


%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Read entry from Ets table.
%% @end
%% --------------------------------------------------------------------
read_ets_int(Key) ->
    Table = someappxyz_ets_tbl,
    case ets:lookup(Table, Key) of
        [] ->
            {error, record_not_found};
        [Rec] ->
            {ok, Rec}
    end.



%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Write entry to Mnesia table, switch process to the gen_server.
%% @end
%% --------------------------------------------------------------------
write_mnesia(Key, Data) ->
    call(?MODULE, write_mnesia_int, [Key, Data]).


%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Write entry to Mnesia table.
%% @end
%% --------------------------------------------------------------------
write_mnesia_int(Key, Data) ->
    Rec = #?SOMEAPPXYZ_MNESIA_TABLE{key = Key, data1 = Data, data2 = erlang:timestamp()},
    Fun = fun() -> mnesia:write(Rec) end,
    case mnesia:sync_transaction(Fun) of
        {atomic, ResultOfFun} ->
            ResultOfFun;
        {aborted, Reason} ->
            {error, Reason}
    end.



%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Read entry from Mnesia table, switch process to the gen_server.
%% @end
%% --------------------------------------------------------------------
read_mnesia(Key) ->
    call(?MODULE, read_mnesia_int, [Key]).


%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Read entry from Mnesia table.
%% @end
%% --------------------------------------------------------------------
read_mnesia_int(Key) ->
    Fun = fun() -> mnesia:read(?SOMEAPPXYZ_MNESIA_TABLE, Key) end,
    case mnesia:sync_transaction(Fun) of
        {atomic, ResultOfFun} ->
            {ok, ResultOfFun};
        {aborted, Reason} ->
            {error, Reason}
    end.


%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Dirty read entry from Mnesia table, switch process to the gen_server.
%%
%% Dirty operations are much faster but it is recommended to only use them for reading data.
%% @end
%% --------------------------------------------------------------------
dirty_read_mnesia(Key) ->
    call(?MODULE, dirty_read_mnesia_int, [Key]).


%% --------------------------------------------------------------------
%% @doc
%% Test/demo function - Dirty read entry from Mnesia table.
%%
%% Dirty operations are much faster but it is recommended to only use them for reading data.
%% @end
%% --------------------------------------------------------------------
dirty_read_mnesia_int(Key) ->
    {ok, mnesia:dirty_read(?SOMEAPPXYZ_MNESIA_TABLE, Key)}.




